-- Server version	5.7.9-log

-- Table structure for table `leagues_teams`
--

DROP TABLE IF EXISTS `leagues_teams`;
CREATE TABLE `leagues_teams` (
  `leagues_id` int(11) NOT NULL,
  `teams_id` int(11) NOT NULL,
  PRIMARY KEY (`leagues_id`,`teams_id`),
  KEY `fk_leagues_has_teams_teams1_idx` (`teams_id`),
  KEY `fk_leagues_has_teams_leagues1_idx` (`leagues_id`),
  CONSTRAINT `fk_leagues_has_teams_leagues1` FOREIGN KEY (`leagues_id`) REFERENCES `leagues` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leagues_has_teams_teams1` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
