-- Server version	5.7.9-log
--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `game` int(11) NOT NULL DEFAULT '0',
  `score` int(11) DEFAULT '0',
  `positions_id` int(11) NOT NULL,
  `teams_id` int(11) NOT NULL,
  `played_games` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`positions_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_players_positions1_idx` (`positions_id`),
  KEY `fk_players_teams1_idx` (`teams_id`),
  CONSTRAINT `fk_players_positions1` FOREIGN KEY (`positions_id`) REFERENCES `positions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_players_teams1` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

