-- ------------------------------------------------------
-- Server version	5.7.9-log
--
-- Table structure for table `teams`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `teams`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `game` int(11) NOT NULL DEFAULT '0',
  `played_games` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

