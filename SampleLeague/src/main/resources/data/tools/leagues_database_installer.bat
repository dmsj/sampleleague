@echo off

REM ############################################
REM ## You can change here your own DB params ##
REM ############################################
REM MYSQL BIN PATH
set mysqlBinPath=C:\Program Files\MySQL\MySQL Server 5.7\bin

set user=
set pass=
set db=
set host=


set mysqldumpPath="%mysqlBinPath%\mysqldump"
set mysqlPath="%mysqlBinPath%\mysql"

echo.
echo.                        leagues database installation
echo.                        __________________________
echo.
echo.
:asklogin
set loginprompt=x
set /p loginprompt=Installation type: (f) full (q) quit? 
if /i %loginprompt%==f goto fullinstall
if /i %loginprompt%==q goto cancel
goto asklogin

REM ############################################
:fullinstall

:validation
set jaja=x
set /p jaja=Are you sure to delete all databases, even characters (y/n) ? 
if /i %jaja%==y goto destruction
if /i %jaja%==n goto cancel
goto validation

:destruction
echo.
echo Start installing.
%mysqlPath% -h %host% -u %user% --password=%pass% -D %db% < ../sql/demo_league_leagues.sql
%mysqlPath% -h %host% -u %user% --password=%pass% -D %db% < ../sql/demo_league_leagues_teams.sql
%mysqlPath% -h %host% -u %user% --password=%pass% -D %db% < ../sql/demo_league_players.sql
%mysqlPath% -h %host% -u %user% --password=%pass% -D %db% < ../sql/demo_league_positions.sql
%mysqlPath% -h %host% -u %user% --password=%pass% -D %db% < ../sql/demo_league_teams.sql
echo Done.
echo.

REM ############################################
:end
echo.
echo Database installation finished.
pause
REM ############################################
:cancel
echo.
echo Database installation canceled.
pause