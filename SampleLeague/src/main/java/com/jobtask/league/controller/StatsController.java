/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.controller;

import com.jobtask.league.config.Config;
import com.jobtask.league.handlers.RankViewHandler;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Team;
import com.jobtask.league.services.StatsService;
import com.jobtask.league.util.RankingUtil;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import com.jobtask.league.model.types.View;

/**
 *
 * @author dmsj
 */
@Named(value = "statsController")
@ViewScoped
public class StatsController implements Serializable {

    @Inject
    private transient StatsService statsService;
    private String currentView;
    private String viewName;
    private List<Team> basketballTeams;
    private List<Team> footballTeams;
    private List<Player> basketballPlayers;
    private List<Player> footballPlayers;
    private List<League> basketballLeagues;
    private List<League> footballLeagues;

    /**
     * Creates a new instance of StatsController
     */
    public StatsController() {
    }

    public List<Team> getBasketballTeams() {
        return basketballTeams;
    }

    public List<Team> getFootballTeams() {
        return footballTeams;
    }

    public List<Player> getBasketballPlayers() {
        return basketballPlayers;
    }

    public List<Player> getFootballPlayers() {
        return footballPlayers;
    }

    public List<League> getBasketballLeagues() {
        return basketballLeagues;
    }

    public List<League> getFootballLeagues() {
        return footballLeagues;
    }

    public String getCurrentView() {
        return currentView;
    }

    public void setCurrentView(View view) {
        resolveCurrentViewName(view);
        this.currentView = RankViewHandler.getView(view);
    }

    public String getViewName() {
        return viewName;
    }

    private void resolveCurrentViewName(View top) {
        switch (top) {
            case TEAM_BASKETBALL_10:
                viewName = "Top basketball teams";
                break;
            case TEAM_FOOTBALL_10:
                viewName = "Top football teams";
                break;
            case LEAGUE_FOOTBALL_10:
                viewName = "Top football leagues";
                break;
            case LEAGUE_BASKETBALL_10:
                viewName = "Top basketball leagues";
                break;
            case PLAYER_FOOTBALL_10:
                viewName = "Top football players";
                break;
            case PLAYER_BASKETBALL_10:
                viewName = "Top basketball players";
                break;
        }
    }

    @PostConstruct
    public void init() {
        //initialize teams
        basketballTeams = statsService.getBasketballTeams();
        footballTeams = statsService.getFootballTeams();
        //calculate teams score (players score sum)
        statsService.resolveTotalPlayersScore(basketballTeams);
        statsService.resolveTotalPlayersScore(footballTeams);
        //when players score sum are attached to teams time to sort and reslove ranks
        RankingUtil.resolveTeamsByScore(basketballTeams, true, Config.LIMIT_TEAMS);
        RankingUtil.resolveTeamsByAverageScore(footballTeams, true, Config.LIMIT_TEAMS);

        //initialize players
        basketballPlayers = statsService.getBasketballPlayers();
        footballPlayers = statsService.getFootballPlayers();
        //sort limit and resolve ranks for football players
        RankingUtil.resolvePlayersByAverageScore(footballPlayers, true, Config.LIMIT_PLAYERS);
        //list should be already limited and sorted so just resolve ranks
        RankingUtil.resolvePlayersByScore(basketballPlayers, false, 0);

        //initialize leagues
        basketballLeagues = statsService.getBasketballLeagues();
        footballLeagues = statsService.getFootballLeagues();
        statsService.resolveTotalTeamsScore(basketballLeagues);
        statsService.resolveAverageScore(footballLeagues);
        RankingUtil.resolveLeaguesByScore(basketballLeagues, true, Config.LIMIT_LEAGUES);
        RankingUtil.resolveLeaguesByAverageScore(footballLeagues, true, Config.LIMIT_LEAGUES);
        //set default view
        setCurrentView(View.TEAM_BASKETBALL_10);
    }
}
