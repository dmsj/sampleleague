/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.controller;

import com.jobtask.league.model.Player;
import com.jobtask.league.model.Position;
import com.jobtask.league.model.Team;
import com.jobtask.league.services.PlayerService;
import com.jobtask.league.services.PositionService;
import com.jobtask.league.services.TeamService;
import com.jobtask.league.util.MessageUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author dmsj
 */
@Named("playerController")
@ViewScoped
public class PlayerController implements Serializable {

    @Inject
    private transient PlayerService playerService;
    @Inject
    private transient PositionService positionService;
    @Inject
    private transient TeamService teamService;
    private List<Player> allPlayers;
    private final Map<Integer, Player> originalPlayers = new HashMap<>();
    private List<Position> allPositions;
    private List<Team> allTeams;
    private Player player;

    /**
     * Creates a new instance of PlayerController
     */
    public PlayerController() {
    }

    public List<Player> getAllPlayers() {
        return allPlayers;
    }

    public List<Position> getAllPositions() {
        return allPositions;
    }

    public List<Team> getAllTeams() {
        return allTeams;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        if (player == null) {
            player = new Player();
            updatePlayer(player);
        }
        return player;
    }

    public void updateAvailablePositions(Player player) {
        playerService.updateAvailablePositions(player, allPositions);
    }

    public void updateAvailableTeams(Player player) {
        playerService.updateAvailableTeams(player, allTeams);
    }

    public void selectedGameChanged(Player player) {
        updateAvailableTeams(player);
        updateAvailablePositions(player);
    }

    private void updatePlayer(Player player) {
        playerService.updateAvailableTeams(player, allTeams);
        player.getAvailablePositions().addAll(positionService.filterPositionsByGame(allPositions, player.getGame()));
    }

    /**
     * Creates new inline player
     */
    public void onCreate() {
        //new line already added do not add new line
        if (allPlayers.contains(player) && playerIsNew(player)) {
            return;
        }
        //set player to null that getter can instantiate new player and update required fields
        player = null;
        getAllPlayers().add(0, getPlayer());
    }

    public String create() {
        //set player to null that getter can instantiate new player and update required fields
        player = null;
        return "create_player";
    }

    public String edit(Player player) {
        //add player to flash that it can be populated in another view page
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("player", player);
        return "edit_player";
    }

    public String update(Player player) {
        saveUpdate(player);
        return "players";
    }

    public void saveUpdate(Player player) {
        Player originalPlayer = originalPlayers.get(player.getId());

        if (originalPlayer != null) {
            if (playerService.isEquals(player, originalPlayer)) {
                MessageUtil.sendInfoMessageToUser("Nothing changed");
                return;
            }
        }
        playerService.update(player);
        MessageUtil.sendInfoMessageToUser(String.format("%s %s has been updated", player.getName(), player.getLastName()));
    }

    public String save() {
        getAllPlayers().add(player);
        MessageUtil.sendInfoMessageToUser(String.format("%s %s has been saved", player.getName(), player.getLastName()));
        playerService.save(player);
        return "players";
    }

    public boolean playerIsNew(Player player) {
        return playerService.playerIsNew(player);
    }

    private void clearInvalidPlayers() {
        List<Player> remove = new ArrayList<>();
        for (Player p : allPlayers) {
            if (playerIsNew(p)) {
                remove.add(p);
            }
        }
        allPlayers.removeAll(remove);
    }

    public void delete(Player player) {

        playerService.delete(player);
        allPlayers.remove(player);
        MessageUtil.sendInfoMessageToUser(String.format("%s %s has been deleted", player.getName(), player.getLastName()));
        clearInvalidPlayers();
    }

    public void onEditInit(RowEditEvent event) {

        Player selectedPlayer = (Player) event.getObject();
        Player player = originalPlayers.get(selectedPlayer.getId());
        if (player != null) {
            originalPlayers.remove(selectedPlayer.getId());
        }
        originalPlayers.put(selectedPlayer.getId(), new Player(selectedPlayer));
    }

    public void onEditCancel(RowEditEvent event) {
        //TODO find better solution for dublicated rows after removing player from players list
        Player player = (Player) event.getObject();
        if (playerIsNew(player) && allPlayers.contains(player)) {
            allPlayers.remove(player);
            try {
                //workaround for dublicated rows in datatable 
                refresh();
            } catch (IOException ex) {
                Logger.getLogger(PlayerController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (originalPlayers.containsKey(player.getId())) {
            originalPlayers.remove(player.getId());
        }
    }

    public void onEditConfirm(RowEditEvent event) {
        Player player = (Player) event.getObject();
        if (player == null) {
            MessageUtil.sendInfoMessageToUser("Failed to update");
            return;
        }
        saveUpdate(player);
    }

    private void refresh() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    @PostConstruct
    public void init() {
        allPlayers = playerService.getAllPlayers();
        allPositions = positionService.getAllPositions();
        allTeams = teamService.getAllTeams();
        allPositions = positionService.extractPositions(allPositions);
        playerService.updateAvailablePositions(allPlayers, allPositions);
        playerService.updateAvailableTeams(allPlayers, allTeams);
    }

    @PreDestroy
    public void clear() {

    }
}
