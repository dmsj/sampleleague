/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.controller;

import com.jobtask.league.config.Config;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.LeagueService;
import com.jobtask.league.services.TeamService;
import com.jobtask.league.util.MessageUtil;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author dmsj
 */
@Named(value = "teamController")
@ViewScoped
public class TeamController implements Serializable {

    @Inject
    transient private TeamService teamService;
    @Inject
    private LeagueService leagueService;
    private List<League> allLeagues;
    private List<Team> allTeams;
    private Team selectedTeam;
    private List<League> availableLeagues;
    private final Map<Integer, Map<League, Boolean>> selectedLeaguesForUpdate = new HashMap<>();
    private League selectedLeague;
    private Set<Integer> editableTeamIds;
    private Game currentGame;
    private boolean editInProgress;
    private Team newTeam;
    private boolean creationInProgress;
    private Team originalTeam;

    public TeamController() {
    }

    public League getSelectedLeague() {
        return selectedLeague;
    }

    public Team getNewTeam() {
        if (newTeam == null) {
            newTeam = new Team();
        }
        return newTeam;
    }

    public void setNewTeam(Team team) {
        newTeam = team;
    }

    public Set<League> getSelectedLeagues() {
        return newTeam.getLeagues();
    }

    public void setSelectedLeague(League selectedLeague) {
        this.selectedLeague = selectedLeague;
    }

    public Team getSelectedTeam() {
        return selectedTeam;
    }

    public boolean isCreationInProgress() {
        return creationInProgress;
    }

    public void setCreationInProgress(boolean creationInProgress) {
        this.creationInProgress = creationInProgress;
    }

    public void setSelectedTeam(Team selectedTeam) {
        this.selectedTeam = selectedTeam;
        updateAvailableLeagues();
    }

    public List<Team> getAllTeams() {
        return allTeams;
    }

    public List<League> getAllLeagues() {
        return allLeagues;
    }

    public List<League> getAvailableLeagues() {
        return availableLeagues;
    }

    public boolean isEditInProgress() {
        return editInProgress;
    }

    public void setEditInProgress(boolean editInProgress) {
        this.editInProgress = editInProgress;
    }

    public void updateAvailableLeagues(Team selectedTeam) {
        availableLeagues.clear();
        if (selectedTeam == null || selectedTeam.getGame() == null) {
            return;
        }
        availableLeagues.addAll(teamService.getAvailableLeagues(selectedTeam.getGame(), new ArrayList(allLeagues)));
        addLeagues(selectedTeam, availableLeagues);
    }

    public void updateAvailableLeagues() {
        Team updateForTeam;
        if (creationInProgress) {
            updateForTeam = newTeam;
        } else {
            updateForTeam = selectedTeam;
        }
        updateAvailableLeagues(updateForTeam);
        addLeagues(updateForTeam, availableLeagues);
    }

    public Map<Integer, Map<League, Boolean>> getSelectedLeaguesForUpdate() {
        return selectedLeaguesForUpdate;
    }

    public void delete(Team team) {
        teamService.delete(team);
        allTeams.remove(team);
        MessageUtil.sendInfoMessageToUser(String.format("Team %s has been deleted", team.getName()));
    }

    public Boolean teamHasLeague(Team team, League league) {
        Map leagues = getSelectedLeaguesForUpdate().get(team.getId());
        if (leagues == null) {
            return null;
        }
        if (leagues.get(league) == null) {
            return false;
        }
        return !(boolean) leagues.get(league);
    }

    public void addLeague(Team team, League league) {

        Boolean hasLeague = teamHasLeague(team, league);
        if (hasLeague == null) {
            Map leaguesMap = new HashMap<>();
            leaguesMap.put(league, false);
            getSelectedLeaguesForUpdate().put(team.getId(), leaguesMap);
            return;
        }
        if (!hasLeague) {
            getSelectedLeaguesForUpdate().get(team.getId()).put(league, false);
        }
    }

    private void addLeagues(Team team, List<League> leagues) {
        //while creating new team all leagues should be available for addLeague method
        if (creationInProgress) {
            return;
        }
        getSelectedLeaguesForUpdate().clear();
        Map lm = new HashMap();
        leagues.stream().filter((l) -> (team.getLeagues().contains(l))).forEach((l) -> {
            lm.put(l, false);
        });
        getSelectedLeaguesForUpdate().put(team.getId(), lm);
    }

    public void removeLeague(Team team, League league) {
        Boolean hasLeague = teamHasLeague(team, league);
        if (hasLeague == null) {
            Map leaguesMap = new HashMap<>();
            leaguesMap.put(league, true);
            getSelectedLeaguesForUpdate().put(team.getId(), leaguesMap);
            return;
        }
        if (hasLeague) {
            getSelectedLeaguesForUpdate().get(team.getId()).put(league, true);
        }
    }

    public boolean teamEditable() {
        if (creationInProgress) {
            return true;
        }
        if (selectedTeam == null) {
            return false;
        }
        return editableTeamIds.contains(selectedTeam.getId());
    }

    public String create() {
        creationInProgress = true;
        return "create_team";
    }

    public void onCreate() {
        newTeam = new Team();
        selectedTeam = newTeam;
        creationInProgress = true;
    }

    public void cancelCreate() {
        newTeam = null;
        creationInProgress = false;
    }

    public String save() {
        save(newTeam);
        return "teams";
    }

    public void save(Team team) {
        updateLeagues(team);
        MessageUtil.sendInfoMessageToUser(String.format("Team %s has been saved", newTeam.getName()));
        teamService.save(team);
        getAllTeams().add(team);
    }

    public String saveUpdate(Team team) {
        update(team);
        return "teams";
    }

    public String edit(Team team) {
        creationInProgress = false;
        updateAvailableLeagues(team);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("team", team);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("leagues", availableLeagues);
        return "edit_team";
    }

    /**
     * Add/Remove leagues saved in selectedLeaguesForUpdate map list
     *
     * @param team
     * @return true if leagues was changed
     */
    private boolean updateLeagues(Team team) {

        Map<League, Boolean> leagues = getSelectedLeaguesForUpdate().get(team.getId());
        Set<League> remove = null;
        if (leagues != null) {
            remove = new HashSet<>();
            for (League l : leagues.keySet()) {
                if (leagues.get(l)) {
                    remove.add(l);
                } else if (!leagues.get(l) && !team.getLeagues().contains(l) && l.getGame() == team.getGame()) {
                    team.getLeagues().add(l);
                }
            }
        }
        if (remove != null && !remove.isEmpty()) {
            team.getLeagues().removeAll(remove);
        }
        return leagues != null && leagues.size() > 0;
    }

    private void update(Team team) {
        editableTeamIds.remove(team.getId());
        //currentGame is updated when current game for team is changed
        //if it doesn't match with current team game remove all leagues
        if (currentGame != null && currentGame != team.getGame()) {
            team.getLeagues().clear();
        }
        //check if leagues was added/removed
        if (!updateLeagues(team)) {
            //if there was no add/remove action with leagues check other teams properties
            if (teamService.isEquals(team, originalTeam)) {
                MessageUtil.sendInfoMessageToUser("Nothing changed");
                return;
            }
        }

        teamService.update(team);
        MessageUtil.sendInfoMessageToUser(team.getName() + " has beed updated");
    }

    public void onEditConfirm(RowEditEvent event) {
        editInProgress = false;

        Team team = ((Team) event.getObject());
        update(team);
    }

    public void onEditCancel(RowEditEvent event) {
        editInProgress = false;
        Team team = ((Team) event.getObject());
        editableTeamIds.remove(team.getId());
    }

    public void onEditInit(RowEditEvent event) {
        editInProgress = true;
        Team team = ((Team) event.getObject());
        editableTeamIds.add(team.getId());
        currentGame = team.getGame();
        originalTeam = new Team(team);
    }

    @PostConstruct
    public void initResources() {
        allTeams = teamService.getAllTeams();
        allLeagues = leagueService.getAllLeagues();
        availableLeagues = new ArrayList<>();
        editableTeamIds = new HashSet<>();
        if (Config.FILTER_LEAGUES_FOR_TEAMS) {
            teamService.filterLeagues(allTeams);
        }
    }

    @PreDestroy
    public void clear() {
    }

}
