/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.controller;

import com.jobtask.league.config.Config;
import com.jobtask.league.dao.TeamDao;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.LeagueService;
import com.jobtask.league.util.MessageUtil;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author dmsj
 */
@Named(value = "leagueController")
@ViewScoped
public class LeagueController implements Serializable {

    /**
     * Creates a new instance of LeagueController
     */
    @Inject
    private transient LeagueService leagueService;
    @Inject
    private transient TeamDao teamDao;

    private League selectedLeague;
    private League createLeague;
    private List<Team> availableTeams;
    private List<Team> allTeams;
    private Set<Team> selectedTeams;
    private List<League> allLeagues;
    private boolean createDialogOpen;
    private boolean editInProgress;
    private League originalLeague;
    private Map<Integer, Set<Team>> selectedTeamsForUpdate;

    public League getCreateLeague() {
        if (createLeague == null) {
            createLeague = new League();
            createLeague.setEditable(true);
        }
        return createLeague;
    }

    public LeagueController() {
    }

    public boolean isEditInProgress() {
        return editInProgress || createDialogOpen;
    }

    public void setEditInProgress(boolean editInProgress) {
        this.editInProgress = editInProgress;
    }

    public League getSelectedLeague() {
        return selectedLeague;
    }

    public void setSelectedLeague(League selectedLeague) {
        this.selectedLeague = selectedLeague;
    }

    public void setCreateDialogOpened(ActionEvent event) {
        createDialogOpen = true;
    }

    public void setCreateDialogClosed(CloseEvent event) {
        createDialogOpen = false;
    }

    public boolean isCreateDialogOpen() {
        return createDialogOpen;
    }

    public Set<Team> getSelectedTeams() {
        return selectedTeams;
    }

    public void setSelectedTeams(Set<Team> selectedTeams) {
        this.selectedTeams = selectedTeams;
    }

    public League getOriginalLeague() {
        return originalLeague;
    }

    public void setOriginalLeague(League originalLeague) {
        this.originalLeague = originalLeague;
    }

    public List<League> getAllLeagues() {
        return allLeagues;
    }

    public List<Team> getAvailableTeams() {
        return availableTeams;
    }

    public void delete(League league) {
        leagueService.delete(league);
        if (getAllLeagues().contains(league)) {
            getAllLeagues().remove(league);
        }
        MessageUtil.sendInfoMessageToUser("League " + league.getName() + " has been deleted");
    }

    public void updateAvailableTeams(League league) {
        if (league != null) {
            updateAvailableTeams(league.getGame());
        }
    }

    public void updateAvailableTeams(Game game) {
        selectedTeams.clear();
        availableTeams.clear();
        availableTeams.addAll(leagueService.getAvailableTeams(game, allTeams));
    }

    public void updateAvailableTeams() {
        updateAvailableTeams(createDialogOpen ? getCreateLeague() : getSelectedLeague());
    }

    public void changeGame(Game game) {
        if (game == null) {
            return;
        }
        updateAvailableTeams(game);
    }

    public void changeGame() {
        changeGame(selectedLeague.getGame());
    }

    public void addTeam(League league, Team team) {
        if (league.getId() == null) {
            selectedTeams.add(team);
            return;
        }
        int key = league.getId();
        Set<Team> teams = selectedTeamsForUpdate.get(key);
        team.setRemove(false);
        if (teams == null) {
            selectedTeamsForUpdate.put(key, new HashSet<>(Arrays.asList(team)));
        } else if (teams.contains(team)) {
            teams.remove(team);
        } else {
            teams.add(team);
        }
    }

    public void removeTeam(League league, Team team) {
        if (league.getId() == null) {
            selectedTeams.remove(team);
            return;
        }
        int key = league.getId();
        Set<Team> teams = selectedTeamsForUpdate.get(key);
        team.setRemove(true);
        if (teams == null) {
            selectedTeamsForUpdate.put(key, new HashSet<>(Arrays.asList(team)));
        } else if (teams.contains(team)) {
            teams.remove(team);
        } else {
            teams.add(team);
        }
    }

    public String saveUpdate(League league) {
        update(league);
        return "leagues";
    }

    public String save() {
        saveLeague();
        return "leagues";
    }

    public String edit(League league) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("league", league);
        updateAvailableTeams(league);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("teams", availableTeams);
        originalLeague = new League(league);
        return "edit_league";
    }

    public void saveLeague() {
        if (!selectedTeams.isEmpty()) {
            getCreateLeague().getTeams().clear();
            getCreateLeague().getTeams().addAll(selectedTeams);
            selectedTeams.clear();
        }
        leagueService.save(getCreateLeague());
        getCreateLeague().setEditable(false);
        MessageUtil.sendInfoMessageToUser(String.format("%s saved", getCreateLeague().getName()));

        //in order to avoid reading from db for renewing entries add new league to leagues list
        if (!getAllLeagues().contains(getCreateLeague())) {
            getAllLeagues().add(getCreateLeague());
        }
    }

    public void editTeams() {
        selectedTeamsForUpdate.clear();
        Set<Team> teams = new HashSet<>(selectedLeague.getTeams());
        originalLeague.setTeams(teams);
    }

    public void cancelEdit() {
        selectedLeague.setEditable(false);
        setEditInProgress(false);
    }

    public void edit() {
        selectedLeague.setEditable(true);
        updateAvailableTeams(selectedLeague);
        setEditInProgress(true);
        setSelectedLeague(selectedLeague);
        originalLeague = new League(selectedLeague);
        //in case if user didn't close teams dialog update edit teams dialog
        editTeams();
    }

    public void update(League league) {
        Set<Team> updateTeams = selectedTeamsForUpdate.get(league.getId());
        Set<Team> remove = null;
        if (originalLeague != null && originalLeague.getGame() != league.getGame()) {
            league.getTeams().clear();
        }
        if (updateTeams != null) {
            remove = new HashSet<>();
            for (Team team : updateTeams) {
                if (team.remove()) {
                    remove.add(team);
                    continue;
                }
                league.getTeams().add(team);
            }
        }
        if (remove != null) {
            league.getTeams().removeAll(remove);
        }

        if (originalLeague != null && leagueService.equals(league, originalLeague)) {
            MessageUtil.sendInfoMessageToUser("Nothing changed");
            return;
        }
        MessageUtil.sendInfoMessageToUser(String.format("%s updated", league.getName()));
        leagueService.update(league);
    }

    public void update() {
        update(selectedLeague);
    }

    public String cancel() {
        return "leagues";
    }

    public String create() {
        createDialogOpen = true;
        return "create_league";
    }

    public void resetFields() {
        selectedTeamsForUpdate.clear();
        createLeague = new League();
        createLeague.setEditable(true);
        selectedTeams.clear();
        availableTeams.clear();
        createDialogOpen = true;
    }

    public Boolean leagueHasTeam(League league, Team team) {

        Set<Team> teams = selectedTeamsForUpdate.get(league.getId());

        if (teams != null) {
            for (Team tm : teams) {
                if (Objects.equals(tm.getId(), team.getId())) {
                    return !tm.remove();
                }
            }
        }
        if (league.isEditable() && selectedTeams.contains(team)) {
            return true;
        }
        return leagueService.leagueHasTeam(league, team);
    }

    @PostConstruct
    public void initResources() {
        allTeams = teamDao.getAllTeams();
        allLeagues = leagueService.getAllLeagues();
        selectedLeague = new League();
        selectedTeams = new HashSet<>();
        selectedTeamsForUpdate = new HashMap<>();
        availableTeams = new ArrayList<>();
        if (Config.FILTER_TEAMS_FOR_LEAGUES) {
            allLeagues.stream().forEach((league) -> {
                leagueService.filterTeams(league);
            });
        }
    }
}
