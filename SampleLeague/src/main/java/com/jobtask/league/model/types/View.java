/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.model.types;

/**
 *
 * @author dmsj
 */
public enum View {
    TEAM_BASKETBALL_10,
    TEAM_FOOTBALL_10,
    LEAGUE_FOOTBALL_10,
    LEAGUE_BASKETBALL_10,
    PLAYER_FOOTBALL_10,
    PLAYER_BASKETBALL_10;
}
