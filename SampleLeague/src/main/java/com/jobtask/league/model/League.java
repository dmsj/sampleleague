package com.jobtask.league.model;
// Generated Mar 23, 2016 8:57:25 PM by Hibernate Tools 4.3.1

import com.jobtask.league.model.types.Game;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * League generated by hbm2java
 */
@Entity
@Table(name = "leagues", catalog = "demo_league"
)
public class League implements java.io.Serializable {

    private Integer id;
    private Game game;
    @NotNull(message = "Field can't be empty")
    @Size(min = 3, max = 28, message = "Length must be between {min} and {max}")
    private String name;
    private Set<Team> teams = new HashSet<>(0);
    private boolean editable = false;
    private int totalScore;
    private double averageScore;
    private String rank;
    private int totalPlayedGames;

    public League() {
    }

    public League(League league) {
        id = league.getId();
        name = league.getName();
        game = league.getGame();
    }

    public League(Game game, String name) {
        this.game = game;
        this.name = name;
    }

    public League(Game game, String name, Set<Team> teamses) {
        this.game = game;
        this.name = name;
        this.teams = teamses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "game", nullable = false)
    public Game getGame() {
        return this.game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            fetch = FetchType.EAGER, targetEntity = Team.class)
    @JoinTable(name = "leagues_teams", inverseJoinColumns = {
        @JoinColumn(name = "teams_id", nullable = false, updatable = false)}, joinColumns = {
        @JoinColumn(name = "leagues_id", nullable = false, updatable = false)})
    public Set<Team> getTeams() {
        return this.teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    @Transient
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Transient
    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    @Transient
    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    @Transient
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Transient
    public int getTotalPlayedGames() {
        return totalPlayedGames;
    }

    public void setTotalPlayedGames(int totalPlayedGames) {
        this.totalPlayedGames = totalPlayedGames;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     * Compares only id
     *
     * @param object the league to compare to
     */
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof League)) {
            return false;
        }
        return Objects.equals(id, ((League) object).getId());
    }

}
