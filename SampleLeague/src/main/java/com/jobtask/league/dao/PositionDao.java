/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao;

import com.jobtask.league.model.Position;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface PositionDao {

    List<Position> getAllPositions();

    Position getPositionById(int id);
}
