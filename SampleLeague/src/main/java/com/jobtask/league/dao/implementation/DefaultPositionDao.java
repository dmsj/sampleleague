/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao.implementation;

import com.jobtask.league.dao.AbstractDao;
import com.jobtask.league.dao.PositionDao;
import com.jobtask.league.model.Position;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultPositionDao extends AbstractDao implements PositionDao {

    @Override
    public List<Position> getAllPositions() {
        List<Position> positions = null;
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            positions = (List<Position>) currentSession().createCriteria(Position.class).list();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return positions;
    }

    @PreDestroy
    public void clearResorses() {
        closeSession();
    }

    @Override
    public Position getPositionById(int id) {
        Transaction tx = null;
        Position pos = null;
        try {
            tx = currentSession().beginTransaction();
            pos = (Position) currentSession().createCriteria(Position.class).add(Restrictions.eq("id", id)).uniqueResult();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return pos;
    }

}
