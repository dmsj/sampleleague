/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao.implementation;

import com.jobtask.league.dao.AbstractDao;
import com.jobtask.league.dao.LeagueDao;
import com.jobtask.league.model.League;
import com.jobtask.league.model.types.Game;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import org.hibernate.Criteria;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultLeagueDao extends AbstractDao implements LeagueDao {

    @Override
    public League getLeagueByID(int id) {
        League league = null;
        try {
            currentSession().beginTransaction();
            Query q = currentSession().createQuery("from League where = :id");
            q.setParameter("id", id);
            league = (League) q.uniqueResult();
            closeSession();
        } catch (NonUniqueResultException e) {
            e.printStackTrace();
            closeSession();
        }

        return league;
    }

    @Override
    public List<League> getAllLeagues() {
        List<League> leagues = null;
        try {
            currentSession().beginTransaction();
            Query q = currentSession().createQuery("from League");
            leagues = (List<League>) q.list();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
        return leagues;
    }

    @Override
    public void save(League league) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(league);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
            e.printStackTrace();
        }
    }

    @Override
    public void update(League league) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(league);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
            e.printStackTrace();
        }
    }

    @Override
    public void delete(League league) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(league);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            closeSession();
        }
    }

    @Override
    public List<League> getLeaguesByGame(Game game) {
        List<League> leagues = null;
        try {
            currentSession().beginTransaction();
            leagues = (List<League>) currentSession().createCriteria(League.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).add(Restrictions.eq("game", game)).list();

            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return leagues;
    }

    @Override
    public List<League> getLeaguesByGame(Game game, int limit, String orderBy) {
        List<League> leagues = null;
        try {
            currentSession().beginTransaction();
            leagues = (List<League>) currentSession().createCriteria(League.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).addOrder(Order.asc(orderBy)).add(Restrictions.eq("game", game)).setMaxResults(limit).list();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return leagues;
    }

    @PreDestroy
    public void clearReasorses() {
        closeSession();
    }
}
