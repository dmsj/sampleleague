/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao;

import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface TeamDao {

    List<Team> getAllTeams();

    Team getTeamById(int id);

    void save(Team team);

    void update(Team team);

    void delete(Team team);

    /**
     * @param game game type
     * @return list of teams filtered by game type
     */
    List<Team> getTeamsByGame(Game game);

    /**
     *
     * @param game game type
     * @param limit limit of records
     * @param orderBy properties to order by
     * @return ordered by players score and limited list of teams filtered by
     * game type
     */
    List<Team> getTeamsByGame(Game game, int limit, String orderBy);

}
