/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao;

import com.jobtask.league.model.Player;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface PlayerDao {

    public List<Player> getAllPlayers();

    public Player getPlayerById(int id);

    public void save(Player player);

    public void update(Player player);

    public void delete(Player player);

    /**
     *
     * @param game game type
     * @return list of players filtered by game type
     */
    List<Player> getPlayersByGame(Game game);

    /**
     *
     * @param game game type
     * @param limit limit of records
     * @param orderBy order properties
     * @return ordered and limited list of players filtered by game type
     */
    List<Player> getPlayersByGame(Game game, int limit, String orderBy);

}
