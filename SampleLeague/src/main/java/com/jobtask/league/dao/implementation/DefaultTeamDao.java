/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao.implementation;

import com.jobtask.league.dao.AbstractDao;
import com.jobtask.league.dao.TeamDao;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultTeamDao extends AbstractDao implements TeamDao {

    @Override
    public List<Team> getAllTeams() {
        List<Team> teams = null;
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            Query q = currentSession().createQuery("from Team");
            teams = (List<Team>) q.list();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
        return teams;
    }

    @Override
    public Team getTeamById(int id) {
        Team team = null;
        try {
            currentSession().beginTransaction();
            team = (Team) currentSession().createCriteria(Team.class).add(Restrictions.eq("id", id)).uniqueResult();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return team;
    }

    @Override
    public void save(Team team) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(team);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
            e.printStackTrace();
        }
    }

    @Override
    public void update(Team team) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().update(team);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
            closeSession();
        }
    }

    @Override
    public void delete(Team team) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(team);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
            e.printStackTrace();
        }
    }

    @Override
    public List<Team> getTeamsByGame(Game game) {
        List<Team> teams = null;
        try {
            currentSession().beginTransaction();
            teams = (List<Team>) currentSession().createCriteria(Team.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).add(Restrictions.eq("game", game)).list();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return teams;
    }

    @Override
    public List<Team> getTeamsByGame(Game game, int limit, String orderBy) {
        List<Team> teams = null;
        try {
            currentSession().beginTransaction();
            teams = (List<Team>) currentSession().createCriteria(Team.class).addOrder(Order.desc(orderBy)).add(Restrictions.eq("game", game)).setMaxResults(limit).list();
            closeSession();
        } catch (Exception e) {
            closeSession();
            e.printStackTrace();
        }
        return teams;
    }

    @PreDestroy
    public void clearResourses() {
        closeSession();
    }
}
