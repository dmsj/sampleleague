/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao;

import com.jobtask.league.model.League;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface LeagueDao {

    League getLeagueByID(int id);

    List<League> getAllLeagues();

    void save(League league);

    void update(League league);

    void delete(League league);

    /**
     * @param game game type
     * @return list of leagues filtered by game type
     */
    List<League> getLeaguesByGame(Game game);

    /**
     *
     * @param game game type
     * @param limit limit of records
     * @param orderBy order properties
     * @return ordered and limited list of leagues filtered by game type
     */
    List<League> getLeaguesByGame(Game game, int limit, String orderBy);
}
