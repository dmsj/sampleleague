/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao.implementation;

import com.jobtask.league.dao.AbstractDao;
import com.jobtask.league.dao.PlayerDao;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.types.Game;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultPlayerDao extends AbstractDao implements PlayerDao {

    @Override
    public List<Player> getAllPlayers() {
        List<Player> players = null;
        try {
            currentSession().beginTransaction();
            Query q = currentSession().createQuery("from Player");
            players = (List<Player>) q.list();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }

        return players;
    }

    @Override
    public Player getPlayerById(int id) {
        Player player = null;
        try {
            currentSession().flush();
            currentSession().beginTransaction();
            player = (Player) currentSession().createCriteria(Player.class).add(Restrictions.eq("id", id)).uniqueResult();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
        return player;
    }

    @Override
    public void save(Player player) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(player);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
        }
    }

    @Override
    public void update(Player player) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(player);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
        }
    }

    @Override
    public void delete(Player player) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(player);
            tx.commit();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            closeSession();
        }
    }

    @Override
    public List<Player> getPlayersByGame(Game game) {
        List<Player> players = null;
        try {
            currentSession().beginTransaction();
            players = (List<Player>) currentSession().createCriteria(Player.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).add(Restrictions.eq("game", game)).list();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
        return players;
    }

    @Override
    public List<Player> getPlayersByGame(Game game, int limit, String orderBy) {
        List<Player> players = null;
        try {
            currentSession().beginTransaction();
            players = (List<Player>) currentSession().createCriteria(Player.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).addOrder(Order.desc(orderBy)).add(Restrictions.eq("game", game)).setMaxResults(limit).list();
            closeSession();
        } catch (Exception e) {
            e.printStackTrace();
            closeSession();
        }
        return players;
    }

    @PreDestroy
    public void clearResorses() {
        closeSession();
    }
}
