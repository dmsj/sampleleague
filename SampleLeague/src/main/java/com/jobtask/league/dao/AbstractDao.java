/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.dao;

import com.jobtask.league.util.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author dmsj
 */
public abstract class AbstractDao {

    private Session session;

    protected Session currentSession() {
        if (session == null) {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        }
        if (!session.isOpen()) {
            session = HibernateUtil.getSessionFactory().openSession();
        }
        return session;
    }

    protected void closeSession() {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }

    protected void close(Session session) {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }

}
