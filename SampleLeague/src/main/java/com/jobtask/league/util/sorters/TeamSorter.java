/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.Team;

/**
 *
 * @author dmsj
 */
public class TeamSorter {

    public static int sortByTotalPlayersScore(Team t1, Team t2) {
        return t2.getTotalPlayersScore() - t1.getTotalPlayersScore();
    }

    public static int sortByAverageScore(Team t1, Team t2) {
        return Double.compare(t2.getAverageScore(), t1.getAverageScore());
    }
}
