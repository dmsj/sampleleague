/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util;

import com.jobtask.league.model.League;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Team;
import com.jobtask.league.util.sorters.LeagueSorter;
import com.jobtask.league.util.sorters.PlayerSorter;
import com.jobtask.league.util.sorters.TeamSorter;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author dmsj
 */
public class RankingUtil {

    /**
     * Set teams ranks by players score sum. Ranks depends on list indexes so
     * list should be sorted or property sort set to true otherwise ranks will
     * be incorrect. Ranks format a-b-n...
     *
     * @param teams
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolveTeamsByScore(List<Team> teams, boolean sort, int limit) {
        if (sort) {
            teams.sort(TeamSorter::sortByTotalPlayersScore);
        }

        if (limit > 0) {
            removeExtraElements(teams, limit);
        }

        int j;
        for (int i = 0; i < teams.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < teams.size() && teams.get(i).getTotalPlayersScore() == teams.get(j).getTotalPlayersScore()) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                teams.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other teams that elements are equal in current loop (except last one that is attached above)
                    teams.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                teams.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Set teams ranks by average score. Ranks depends on list indexes so list
     * should be sorted or property sort set to true otherwise ranks will be
     * incorrect. Ranks format a-b-n...
     *
     * @param teams
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolveTeamsByAverageScore(List<Team> teams, boolean sort, int limit) {
        if (sort) {
            teams.sort(TeamSorter::sortByAverageScore);
        }

        if (limit > 0) {
            removeExtraElements(teams, limit);
        }

        int j;
        for (int i = 0; i < teams.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < teams.size() && teams.get(i).getAverageScore() == teams.get(j).getAverageScore()) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                teams.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other teams that elements are equal in current loop (except last one that is attached above)
                    teams.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                teams.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Set players ranks by score. Ranks depends on list indexes so list should
     * be sorted or property sort set to true otherwise ranks will be incorrect.
     * Ranks format a-b-n...
     *
     * @param players
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolvePlayersByScore(List<Player> players, boolean sort, int limit) {
        if (sort) {
            players.sort(PlayerSorter::sortByScore);
        }

        if (limit > 0) {
            removeExtraElements(players, limit);
        }
        int j;
        for (int i = 0; i < players.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < players.size() && Objects.equals(players.get(i).getScore(), players.get(j).getScore())) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                players.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other players that elements are equal in current loop (except last one that is attached above)
                    players.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                players.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Set players ranks by score. Ranks depends on list indexes so list should
     * be sorted or property sort set to true otherwise ranks will be incorrect.
     * Ranks format a-b-n...
     *
     * @param players
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolvePlayersByAverageScore(List<Player> players, boolean sort, int limit) {
        if (sort) {
            players.sort(PlayerSorter::sortByAverageScore);
        }

        if (limit > 0) {
            removeExtraElements(players, limit);
        }
        int j;
        for (int i = 0; i < players.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < players.size() && players.get(i).getAverageScore() == players.get(j).getAverageScore()) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                players.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other players that elements are equal in current loop (except last one that is attached above)
                    players.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                players.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Set leagues ranks by total teams score. Ranks depends on list indexes so
     * list should be sorted or property sort set to true otherwise ranks will
     * be incorrect. Ranks format a-b-n...
     *
     * @param leagues
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolveLeaguesByScore(List<League> leagues, boolean sort, int limit) {
        if (sort) {
            leagues.sort(LeagueSorter::sortByTotalScore);
        }

        if (limit > 0) {
            removeExtraElements(leagues, limit);
        }
        int j;
        for (int i = 0; i < leagues.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < leagues.size() && leagues.get(i).getTotalScore() == leagues.get(j).getTotalScore()) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                leagues.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other leagues that elements are equal in current loop (except last one that is attached above)
                    leagues.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                leagues.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Set leagues ranks by average teams score. Ranks depends on list indexes
     * so list should be sorted or property sort set to true otherwise ranks
     * will be incorrect. Ranks format a-b-n...
     *
     * @param leagues
     * @param sort if true list will be sorted before resolving ranks
     * @param limit removes elements from list if list size is higher than
     * limit. Last elements of the list will be removed.
     */
    public static void resolveLeaguesByAverageScore(List<League> leagues, boolean sort, int limit) {
        if (sort) {
            leagues.sort(LeagueSorter::sortByAverageScore);
        }

        if (limit > 0) {
            removeExtraElements(leagues, limit);
        }
        int j;
        for (int i = 0; i < leagues.size(); i++) {
            int temp = 1 + i;
            j = temp;
            StringBuilder rank = new StringBuilder();
            rank.append(temp);
            while (j < leagues.size() && leagues.get(i).getAverageScore() == leagues.get(j).getAverageScore()) {
                //while next element of list are equals with first one that is compared with add list indexes as rank 
                ++j;
                temp = j;
                rank.append("-");
                rank.append(temp);
            }
            if (j > 1 + i) {//this means that there was at least two equal elements 
                //j constant now holds last+1 index of equal elements in current loop
                //i constant holds first element index that was compared in current loop
                leagues.get(j - 1).setRank(rank.toString());
                while (j != 1 + i) {
                    //attache ranks to other leagues that elements are equal in current loop (except last one that is attached above)
                    leagues.get(i).setRank(rank.toString());
                    i++;
                }
            } else {
                leagues.get(i).setRank(rank.toString());
            }
        }
    }

    /**
     * Remove extra elements from list if list size is greater that limit.
     * Elements will be removed from list end.
     */
    private static void removeExtraElements(List list, int limit) {
        if (limit >= list.size()) {
            return;
        }
        list.subList(limit, list.size()).clear();
    }
}
