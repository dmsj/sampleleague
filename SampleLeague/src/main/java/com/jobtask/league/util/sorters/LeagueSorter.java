/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.League;

/**
 *
 * @author dmsj
 */
public class LeagueSorter {

    public static int sortByTotalScore(League l1, League l2) {
        return l2.getTotalScore() - l1.getTotalScore();
    }

    public static int sortByAverageScore(League l1, League l2) {
        return Double.compare(l2.getAverageScore(), l1.getAverageScore());
    }
}
