/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util;

import com.jobtask.league.model.types.Game;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author dmsj
 */
@Named(value = "basicUtil")
@ApplicationScoped
public class BasicUtilBean {

    /**
     * Creates a new instance of BasicUtilBean
     */
    public BasicUtilBean() {
    }

    public Game[] getGames() {
        return Game.values();
    }

    public List<Game> getGames(Game exclude) {
        return Arrays.stream(Game.values()).filter(g -> !g.equals(exclude)).collect(Collectors.toList());
    }

}
