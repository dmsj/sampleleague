/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.Player;

/**
 *
 * @author dmsj
 */
public class PlayerSorter {

    public static int sortByScore(Player p1, Player p2) {
        return p2.getScore() - p1.getScore();
    }

    public static int sortByAverageScore(Player p1, Player p2) {
        return Double.compare(p2.getAverageScore(), p1.getAverageScore());
    }
}
