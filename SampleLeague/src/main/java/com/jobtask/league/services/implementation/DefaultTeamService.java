/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.config.Config;
import com.jobtask.league.dao.TeamDao;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.TeamService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultTeamService implements TeamService {

    @Inject
    TeamDao teamDao;

    @Override
    public List<Team> getAllTeams() {
        return teamDao.getAllTeams();
    }

    @Override
    public Team getTeamById(int id) {
        return teamDao.getTeamById(id);
    }

    @Override
    public void save(Team team) {
        teamDao.save(team);
    }

    @Override
    public void update(Team team) {
        teamDao.update(team);
    }

    @Override
    public boolean hasLeague(Team team, League league) {
        return team.getLeagues().stream().noneMatch(t -> t.getId().equals(league.getId()));
    }

    @Override
    public Set<League> getAvailableLeagues(Game game, List<League> leagues) {
        Set<League> availableLeagues = new HashSet<>();
        if (leagues != null) {
            leagues.stream().filter((league) -> (league.getGame() == game)).forEach((league) -> {
                availableLeagues.add(league);
            });
        }
        return availableLeagues;
    }

    @Override
    public void delete(Team team) {
        teamDao.delete(team);
    }

    @Override
    public void filterLeagues(Team team) {
        List<League> validLeagues = new ArrayList<>();
        team.getLeagues().stream().filter((league) -> (league.getGame() == team.getGame())).forEach((league) -> {
            validLeagues.add(league);
        });
        if (validLeagues.size() != team.getLeagues().size()) {
            team.getLeagues().clear();
            team.getLeagues().addAll((validLeagues));
            if (Config.FILTER_CLEAN_INVALID_GAMETYPES) {
                update(team);
            }
        }
    }

    @Override
    public void filterLeagues(List<Team> teams) {
        teams.stream().forEach((team) -> {
            List<League> validTeams = new ArrayList<>();
            team.getLeagues().stream().filter((league) -> (league.getGame() == team.getGame())).forEach((league) -> {
                validTeams.add(league);
            });
            team.getLeagues().clear();
            team.getLeagues().addAll((validTeams));
        });
    }

    @Override
    public List<Team> filterTeamsByGame(List<Team> teams, Game game) {
        List<Team> available = new ArrayList<>();
        teams.stream().filter(t -> t.getGame() == game).forEach(t -> {
            available.add(t);
        });
        return available;
    }

    /**Compares teams games, played games count, names.
     * @param tm1
     * @param tm2
     * @return true if teams are equal*/
    @Override
    public boolean isEquals(Team tm1, Team tm2) {
        if (tm1.getGame() != tm2.getGame()
                || !Objects.equals(tm1.getPlayedGames(), tm2.getPlayedGames())
                || !tm1.getName().equalsIgnoreCase(tm2.getName())) {
            return false;
        }
        return true;
    }
}
