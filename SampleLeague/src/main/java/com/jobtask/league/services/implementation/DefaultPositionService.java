/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.dao.PositionDao;
import com.jobtask.league.model.Position;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.PositionService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultPositionService implements PositionService {

    @Inject
    PositionDao positionDao;

    @Override
    public List<Position> getAllPositions() {
        return positionDao.getAllPositions();
    }

    @Override
    public List<Position> filterPositionsByGame(List<Position> positions, Game game) {
        List<Position> available = new ArrayList<>();
        for (Position pos : positions) {
            if (pos.getCurrentGames().contains(game)) {
                available.add(pos);
            }
        }
        return available;
    }

    @Override
    public List<Position> extractPositions(List<Position> positions) {
        for (Position pos : positions) {
            pos.setCurrentGames(extractPositions(pos));
        }
        return positions;
    }

    @Override
    public Position getPositionById(int id) {
        return positionDao.getPositionById(id);
    }

    @Override
    public List<Game> extractPositions(Position position) {
        List<Game> games = new ArrayList<>();
        List<String> splitted = Arrays.asList(position.getGames().split(","));
        for (String game : splitted) {
            for (Game gt : Game.values()) {
                if (gt.ordinal() == Integer.parseInt(game)) {
                    games.add(gt);
                    break;
                }
            }
        }
        return games;
    }
}
