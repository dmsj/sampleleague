/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services;

import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface LeagueService {

    public List<League> getAllLeagues();

    public void update(League league);

    public void save(League league);

    public League getLeagueById(int id);

    public List<Team> getAvailableTeams(Game game, List<Team> teams);

    public void filterTeams(League league);

    public Boolean leagueHasTeam(League league, Team team);

    public void delete(League league);

    public boolean equals(League l1, League l2);
}
