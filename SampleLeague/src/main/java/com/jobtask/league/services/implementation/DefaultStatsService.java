/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.config.Config;
import com.jobtask.league.dao.LeagueDao;
import com.jobtask.league.dao.PlayerDao;
import com.jobtask.league.dao.TeamDao;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.StatsService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultStatsService implements StatsService {

    @Inject
    private transient LeagueDao leagueDao;
    @Inject
    private transient TeamDao teamDao;
    @Inject
    private transient PlayerDao playerDao;

    /**
     * @return limited and ordered by score list of basketball players
     */
    @Override
    public List<Player> getBasketballPlayers() {
        return playerDao.getPlayersByGame(Game.Basketball, Config.LIMIT_PLAYERS, "score");
    }

    @Override
    public List<Player> getFootballPlayers() {
        return playerDao.getPlayersByGame(Game.Football);
    }

    @Override
    public List<Team> getBasketballTeams() {
        return teamDao.getTeamsByGame(Game.Basketball);
    }

    @Override
    public List<Team> getFootballTeams() {
        return teamDao.getTeamsByGame(Game.Football);
    }

    @Override
    public List<League> getBasketballLeagues() {
        return leagueDao.getLeaguesByGame(Game.Basketball);
    }

    @Override
    public List<League> getFootballLeagues() {
        return leagueDao.getLeaguesByGame(Game.Football);
    }

    @Override
    public void resolveTotalPlayersScore(Team team) {
        team.setTotalScore(team.getPlayers().stream().mapToInt(p -> p.getScore()).sum());
    }

    @Override
    public void resolveTotalPlayersScore(List<Team> teams) {
        teams.forEach((t) -> {
            t.setTotalScore(t.getPlayers().stream().mapToInt(p -> p.getScore()).sum());
        });
    }

    @Override
    public void resolveTotalTeamsScore(League league) {
        resolveTotalPlayersScore(new ArrayList<>(league.getTeams()));
        league.setTotalScore(league.getTeams().stream().mapToInt(t -> t.getTotalPlayersScore()).sum());
    }

    @Override
    public void resolveTotalTeamsScore(List<League> leagues) {
        leagues.forEach((league) -> {
            resolveTotalTeamsScore(league);
        });
    }

    @Override
    public void resolveTotalPlayedGames(League league) {
        league.setTotalPlayedGames(league.getTeams().stream().mapToInt(t -> t.getPlayedGames()).sum());
    }

    @Override
    public void resolveAverageScore(List<League> leagues) {
        leagues.forEach((league) -> {
            resolveAverageScore(league);
        });
    }

    @Override
    public void resolveAverageScore(League league) {
        resolveTotalTeamsScore(league);
        resolveTotalPlayedGames(league);
        double average;
        if (league.getTotalPlayedGames() < 1) {
            average = 0;
        } else {
            average = league.getTotalScore() / league.getTotalPlayedGames();
        }
        league.setAverageScore(average);
    }
}
