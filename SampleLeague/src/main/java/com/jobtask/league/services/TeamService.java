/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services;

import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import java.util.List;
import java.util.Set;

/**
 *
 * @author dmsj
 */
public interface TeamService {

    List<Team> getAllTeams();

    Team getTeamById(int id);

    void save(Team team);

    void update(Team team);

    boolean hasLeague(Team team, League league);

    Set<League> getAvailableLeagues(Game game, List<League> leagues);

    void delete(Team team);

    void filterLeagues(Team team);

    void filterLeagues(List<Team> teams);

    List<Team> filterTeamsByGame(List<Team> teams, Game game);
    
    boolean isEquals(Team tm1, Team tm2);
}
