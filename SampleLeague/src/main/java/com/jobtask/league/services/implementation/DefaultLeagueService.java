/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.config.Config;
import com.jobtask.league.dao.LeagueDao;
import com.jobtask.league.model.League;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.LeagueService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultLeagueService implements LeagueService {

    @Inject
    private transient LeagueDao leagueDao;

    @Override
    public List<League> getAllLeagues() {
        return leagueDao.getAllLeagues();
    }

    @Override
    public void update(League league) {
        leagueDao.update(league);
    }

    @Override
    public void save(League league) {
        leagueDao.save(league);
    }

    @Override
    public void delete(League league) {
        leagueDao.delete(league);
    }

    @Override
    public League getLeagueById(int id) {
        return leagueDao.getLeagueByID(id);
    }

    @Override
    public List<Team> getAvailableTeams(Game game, List<Team> teams) {
        List<Team> availableTeams = new ArrayList<>();
        if (teams != null) {
            teams.stream().filter((team) -> (team.getGame() == game)).forEach((team) -> {
                availableTeams.add(team);
            });
        }
        return availableTeams;
    }

    @Override
    public void filterTeams(League league) {
        List<Team> validTeams = new ArrayList<>();
        league.getTeams().stream().filter((team) -> (team.getGame() == league.getGame())).forEach((team) -> {
            validTeams.add(team);
        });
        if (validTeams.size() != league.getTeams().size()) {
            league.getTeams().clear();
            league.getTeams().addAll((validTeams));
            if (Config.FILTER_CLEAN_INVALID_GAMETYPES) {
                update(league);
            }
        }
    }

    @Override
    public Boolean leagueHasTeam(League league, Team team) {
        if (league == null || team == null) {
            return null;
        }
        if (league.getId() == null || team.getId() == null) {
            return null;
        }
        return !league.getTeams().stream().noneMatch(t -> t.getId().equals(team.getId()));
    }

    /**
     * Compares name,game and teams
     *
     * @param l1 the league1
     * @param l2 the league2
     * @return true if leagues are equal
     */
    @Override
    public boolean equals(League l1, League l2) {
        if (!(l1.getName().equalsIgnoreCase(l2.getName())) || l1.getGame() != l2.getGame() || l1.getTeams().size() != l2.getTeams().size() || !(l1.getTeams().containsAll(l2.getTeams()))) {
            return false;
        }
        return true;
    }

}
