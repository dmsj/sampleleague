/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services;

import com.jobtask.league.model.Position;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface PositionService {

    List<Position> getAllPositions();

    List<Position> filterPositionsByGame(List<Position> positions, Game game);

    List<Position> extractPositions(List<Position> positions);

    List<Game> extractPositions(Position position);

    Position getPositionById(int id);

}
