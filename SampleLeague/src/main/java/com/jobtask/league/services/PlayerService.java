/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services;

import com.jobtask.league.model.Player;
import com.jobtask.league.model.Position;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import java.util.List;

/**
 *
 * @author dmsj
 */
public interface PlayerService {

    List<Player> getAllPlayers();

    Player getPlayerById(int id);

    void save(Player player);

    void update(Player player);

    void delete(Player player);

    boolean isEquals(Player p1, Player p2);

    List<Position> getAvailablePositions(Game game);

    List<Team> getAvailableTeams(Game game);

    void updateAvailablePositions(List<Player> players, List<Position> positions);

    void updateAvailableTeams(List<Player> players, List<Team> teams);

    void updateAvailableTeams(Player player, List<Team> teams);

    void updateAvailablePositions(Player player, List<Position> positions);

    boolean playerIsNew(Player player);
}
