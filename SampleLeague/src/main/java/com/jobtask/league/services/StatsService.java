/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services;

import com.jobtask.league.model.League;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Team;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dmsj
 */
@Local
public interface StatsService {

    List<Player> getBasketballPlayers();

    List<Player> getFootballPlayers();

    List<Team> getBasketballTeams();

    List<Team> getFootballTeams();

    List<League> getBasketballLeagues();

    List<League> getFootballLeagues();

    void resolveTotalPlayersScore(Team team);

    void resolveTotalPlayersScore(List<Team> teams);

    void resolveTotalTeamsScore(League league);

    void resolveTotalTeamsScore(List<League> leagues);

    void resolveTotalPlayedGames(League league);

    void resolveAverageScore(List<League> leagues);

    void resolveAverageScore(League league);
}
