/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.dao.PlayerDao;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Position;
import com.jobtask.league.model.Team;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.PlayerService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dmsj
 */
@Stateless
public class DefaultPlayerService implements PlayerService {

    @Inject
    PlayerDao playerDao;

    @Override
    public List<Player> getAllPlayers() {
        return playerDao.getAllPlayers();
    }

    @Override
    public Player getPlayerById(int id) {
        return playerDao.getPlayerById(id);
    }

    @Override
    public void save(Player player) {
        playerDao.save(player);
    }

    @Override
    public void update(Player player) {
        playerDao.update(player);
    }

    @Override
    public void delete(Player player) {
        playerDao.delete(player);
    }

    /**
     * Compares players names,last names, game, played games,
     * position,score,team
     *
     * @param p1 the player1
     * @param p2 the player 2
     * @return false if one of compared criteria is not equal
     */
    @Override
    public boolean isEquals(Player p1, Player p2) {
        return !(!(p1.getName().equalsIgnoreCase(p2.getName()))
                || !(p1.getLastName().equalsIgnoreCase(p2.getLastName()))
                || p1.getGame() != p2.getGame()
                || p1.getPlayedGames() != p2.getPlayedGames()
                || !Objects.equals(p1.getPosition().getId(), p2.getPosition().getId())
                || !Objects.equals(p1.getScore(), p2.getScore())
                || !Objects.equals(p1.getTeam().getId(), p2.getTeam().getId()));
    }

    @Override
    public List<Position> getAvailablePositions(Game game) {
        List<Position> available = new ArrayList<>();

        return available;
    }

    @Override
    public List<Team> getAvailableTeams(Game game) {
        List<Team> available = new ArrayList<>();

        return available;
    }

    @Override
    public void updateAvailablePositions(List<Player> players, List<Position> positions) {
        for (Player player : players) {
            player.getAvailablePositions().clear();
            for (Position pos : positions) {
                if (pos.getCurrentGames().contains(player.getGame())) {
                    player.getAvailablePositions().add(pos);
                }
            }
        }
    }

    @Override
    public void updateAvailablePositions(Player player, List<Position> positions) {
        player.getAvailablePositions().clear();
        for (Position pos : positions) {
            if (pos.getCurrentGames().contains(player.getGame())) {
                player.getAvailablePositions().add(pos);
            }
        }
    }

    @Override
    public void updateAvailableTeams(List<Player> players, List<Team> teams) {
        for (Player player : players) {
            player.getAvailableTeams().clear();
            for (Team team : teams) {
                if (player.getGame() == team.getGame()) {
                    player.getAvailableTeams().add(team);
                }
            }
        }
    }

    @Override
    public void updateAvailableTeams(Player player, List<Team> teams) {
        player.getAvailableTeams().clear();
        for (Team team : teams) {
            if (player.getGame() == team.getGame()) {
                player.getAvailableTeams().add(team);
            }
        }
    }

    @Override
    public boolean playerIsNew(Player player) {
        return player == null || player.getName() == null || player.getLastName() == null || player.getName().isEmpty() || player.getLastName().isEmpty() || player.getTeam() == null || player.getPosition() == null;
    }
}
