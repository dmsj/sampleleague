/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.converters;

import com.jobtask.league.model.Team;
import com.jobtask.league.services.TeamService;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author dmsj
 */
@FacesConverter(forClass = Team.class)
public class TeamConverter implements Converter {

    private final TeamService teamService;

    public TeamConverter() {
        teamService = CDI.current().select(TeamService.class).get();
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Team team = null;
        try {
            team = new Team(teamService.getTeamById(Integer.valueOf(value)));
        } catch (NumberFormatException | NullPointerException e) {
            throw new ConverterException(new FacesMessage(value + " is not a valid Team ID"), e);
        }
        return team;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof Team) {
            return String.valueOf(((Team) value).getId());
        } else {
            throw new ConverterException(new FacesMessage(value + " is not a valid Position"));
        }
    }

}
