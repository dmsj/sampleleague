/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.converters;

import com.jobtask.league.model.Position;
import com.jobtask.league.services.PositionService;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author dmsj
 */
@FacesConverter(forClass = Position.class)
public class PositionConverter implements Converter {

    private final PositionService service;

    public PositionConverter() {
        service = CDI.current().select(PositionService.class).get();
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Position pos = null;
        try {
            pos = new Position(service.getPositionById(Integer.valueOf(value)));
        } catch (NumberFormatException | NullPointerException e) {
            throw new ConverterException(new FacesMessage(value + " is not a valid Position ID"), e);
        }
        return pos;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof Position) {
            return String.valueOf(((Position) value).getId());
        } else {
            throw new ConverterException(new FacesMessage(value + " is not a valid Position"));
        }
    }

}
