/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.handlers;

import com.jobtask.league.model.types.View;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dmsj
 */
public class RankViewHandler {

    private static final Map<View, String> handler = new HashMap();

    public static void register() {
        handler.put(View.TEAM_BASKETBALL_10, "/views/tops/top10_team_basketball.xhtml");
        handler.put(View.TEAM_FOOTBALL_10, "/views/tops/top10_team_football.xhtml");

        handler.put(View.PLAYER_BASKETBALL_10, "/views/tops/top10_player_basketball.xhtml");
        handler.put(View.PLAYER_FOOTBALL_10, "/views/tops/top10_player_football.xhtml");

        handler.put(View.LEAGUE_BASKETBALL_10, "/views/tops/top10_league_basketball.xhtml");
        handler.put(View.LEAGUE_FOOTBALL_10, "/views/tops/top10_league_football.xhtml");
    }

    public static String getView(View top) {
        return handler.get(top);
    }

}
