/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dmsj
 */
public class Config {

    private static final String PROPERTIES = "config.properties";
    /**
     * if league game and team game not match team will not be added to list
     */
    public static boolean FILTER_TEAMS_FOR_LEAGUES;
    /**
     * if league game and team game not match team will not be added to list
     */
    public static boolean FILTER_LEAGUES_FOR_TEAMS;
    /**
     * if league and team gameType will not match league or/and team will be
     * removed from lists and db
     */
    public static boolean FILTER_CLEAN_INVALID_GAMETYPES;
    /**
     * max players that will be retrieved from db
     */
    public static int LIMIT_PLAYERS;
    /**
     * max teams that will be retrieved from db
     */
    public static int LIMIT_TEAMS;
    /**
     * max leagues that will be retrieved from db
     */
    public static int LIMIT_LEAGUES;

    public static void load() {
        Properties cfg = loadProperties(PROPERTIES);

        FILTER_TEAMS_FOR_LEAGUES = Boolean.parseBoolean(cfg.getProperty("FilterTeamsForLeagues", "True"));
        FILTER_LEAGUES_FOR_TEAMS = Boolean.parseBoolean(cfg.getProperty("FilterLeaguesForTeams", "True"));
        FILTER_CLEAN_INVALID_GAMETYPES = Boolean.parseBoolean(cfg.getProperty("CleanOnGameTypeFilterViolation", "True"));
        LIMIT_PLAYERS = Integer.parseInt(cfg.getProperty("LimitPlayers", "10"));
        LIMIT_TEAMS = Integer.parseInt(cfg.getProperty("LimitTeams", "10"));
        LIMIT_LEAGUES = Integer.parseInt(cfg.getProperty("LimitLeagues", "10"));
    }

    private static Properties loadProperties(String file) {
        Properties cfg = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        try (InputStream is = classLoader.getResourceAsStream(file)) {
            cfg.load(is);
            is.close();
        } catch (IOException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cfg;
    }
}
