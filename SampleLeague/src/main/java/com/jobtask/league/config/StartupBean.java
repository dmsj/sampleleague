/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.config;

import com.jobtask.league.handlers.RankViewHandler;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author dmsj
 */
@Startup
@Singleton
public class StartupBean {

    public enum States {
       STARTED, PAUSED, SHUTTING_DOWN
    };
    private States state;

    @PostConstruct
    public void start() {
        // Perform intialization
        state = States.STARTED;
        System.out.println("<-------------->Application state changed--------->" + getState());
        Config.load();
        RankViewHandler.register();
    }

    @PreDestroy
    public void stop() {
        state = States.SHUTTING_DOWN;
        // Perform termination
        System.out.println("<-------------->Application state changed--------->" + getState());
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }
}
