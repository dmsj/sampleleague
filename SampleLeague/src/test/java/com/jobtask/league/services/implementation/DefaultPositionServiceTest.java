/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.services.implementation;

import com.jobtask.league.model.Position;
import com.jobtask.league.model.types.Game;
import com.jobtask.league.services.PositionService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmsj
 */
public class DefaultPositionServiceTest {

    private PositionService positionService = new DefaultPositionService();

    private List<Position> _positions;

    public DefaultPositionServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        _positions = new ArrayList<>();
        Position pos1 = new Position();
        Position pos2 = new Position();
        Position pos3 = new Position();
        Position pos4 = new Position();
        pos1.setName("pos1");
        pos2.setName("pos2");
        pos3.setName("pos3");
        pos4.setName("pos4");
        pos1.setGames("0");
        pos2.setGames("0,1");
        pos3.setGames("1");
        pos4.setGames("0,1");
        pos1.setCurrentGames(new ArrayList<Game>() {
            {
                add(Game.Basketball);
            }
        });
        pos2.setCurrentGames(new ArrayList<Game>() {
            {
                add(Game.Football);
                add(Game.Basketball);
            }
        });
        pos3.setCurrentGames(new ArrayList<Game>() {
            {
                add(Game.Basketball);
            }
        });
        pos4.setCurrentGames(new ArrayList<Game>() {
            {
                add(Game.Football);
            }
        });
        _positions.add(pos1);
        _positions.add(pos2);
        _positions.add(pos3);
        _positions.add(pos4);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of filterPositionsByGame method, of class DefaultPositionService.
     */
    @Test
    public void testFilterPositionsByGame() throws Exception {
        System.out.println("filterPositionsByGame");
        List<Position> positions;

        Game game = Game.Football;
        positions = positionService.filterPositionsByGame(_positions, game);

        int result = positions.size();
        //two positions should be available for football game
        assertEquals(2, result);

        game = Game.Basketball;
        positions = positionService.filterPositionsByGame(_positions, game);
        result = positions.size();
        //three positions should be available for basketball game
        assertEquals(3, result);
    }

    /**
     * Test of extractPositions method, of class DefaultPositionService.
     */
    @Test
    public void testExtractPositions() throws Exception {
        System.out.println("extractPositions");
        Position pos = _positions.get(0);
        Position pos2 = _positions.get(1);
        pos.setCurrentGames(positionService.extractPositions(pos));
        pos2.setCurrentGames(positionService.extractPositions(pos2));

        //pos current games should contain only football game (0)
        assertTrue(pos.getCurrentGames().contains(Game.Football));
        assertFalse(pos.getCurrentGames().contains(Game.Basketball));

        //pos2 current games should contain football and basketball games
        assertTrue(pos2.getCurrentGames().contains(Game.Football));
        assertTrue(pos2.getCurrentGames().contains(Game.Basketball));
    }
}
