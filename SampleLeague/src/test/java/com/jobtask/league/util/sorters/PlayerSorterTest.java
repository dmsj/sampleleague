/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.Player;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmsj
 */
public class PlayerSorterTest {

    public PlayerSorterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sortByScore method, of class PlayerSorter.
     */
    @Test
    public void testSortByScore() {
        System.out.println("sortByScore");
        Player p1 = new Player();
        Player p2 = new Player();
        Player p3 = new Player();
        Player p4 = new Player();
        p1.setName("p1");
        p1.setScore(2);
        p1.setPlayedGames(8);
        p2.setName("p2");
        p2.setScore(25);
        p2.setPlayedGames(1);
        p3.setName("p3");
        p3.setScore(10);
        p3.setPlayedGames(2);
        p4.setName("p4");
        p4.setScore(3);
        p4.setPlayedGames(2);
        List<Player> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        assertEquals("p1", players.get(0).getName());
        assertEquals("p4", players.get(players.size() - 1).getName());
        
        players.sort(PlayerSorter::sortByScore);

        assertEquals("p2", players.get(0).getName());
        assertEquals("p1", players.get(players.size() - 1).getName());
    }

    /**
     * Test of sortByAverageScore method, of class PlayerSorter.
     */
    @Test
    public void testSortByAverageScore() {
        System.out.println("sortByAverageScore");
        Player p1 = new Player();
        Player p2 = new Player();
        Player p3 = new Player();
        Player p4 = new Player();
        p1.setName("p1");
        p1.setScore(10);
        p1.setPlayedGames(8);
        p2.setName("p2");
        p2.setScore(25);
        p2.setPlayedGames(1);
        p3.setName("p3");
        p3.setScore(10);
        p3.setPlayedGames(2);
        p4.setName("p4");
        p4.setScore(15);
        p4.setPlayedGames(2);
        List<Player> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        assertEquals("p1", players.get(0).getName());
        assertEquals("p4", players.get(players.size() - 1).getName());

        players.sort(PlayerSorter::sortByAverageScore);

        assertEquals("p2", players.get(0).getName());
        assertEquals("p1", players.get(players.size() - 1).getName());
    }

}
