/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.League;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmsj
 */
public class LeagueSorterTest {

    List<League> _leagues;

    public LeagueSorterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        _leagues = new ArrayList<>();

        League l1 = new League();
        League l2 = new League();
        League l3 = new League();
        League l4 = new League();
        League l5 = new League();
        l1.setName("l1");
        l2.setName("l2");
        l3.setName("l3");
        l4.setName("l4");
        l5.setName("l5");
        l1.setAverageScore(10);
        l2.setAverageScore(15);
        l3.setAverageScore(5);
        l4.setAverageScore(33);
        l5.setAverageScore(41);
        l1.setTotalScore(10);
        l2.setTotalScore(15);
        l3.setTotalScore(41);
        l4.setTotalScore(20);
        l5.setTotalScore(5);
        _leagues.add(l1);
        _leagues.add(l2);
        _leagues.add(l3);
        _leagues.add(l4);
        _leagues.add(l5);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sortByTotalScore method, of class LeagueSorter.
     */
    @Test
    public void testSortByTotalScore() {
        System.out.println("sortByTotalScore");
        List<League> leagues = _leagues;
        leagues.sort(LeagueSorter::sortByTotalScore);
        assertEquals("l3", leagues.get(0).getName());
        assertEquals("l4", leagues.get(1).getName());
        assertEquals("l2", leagues.get(2).getName());
        assertEquals("l1", leagues.get(3).getName());
        assertEquals("l5", leagues.get(4).getName());
    }

    /**
     * Test of sortByAverageScore method, of class LeagueSorter.
     */
    @Test
    public void testSortByAverageScore() {
        List<League> leagues = _leagues;
        leagues.sort(LeagueSorter::sortByAverageScore);
        assertEquals("l5", leagues.get(0).getName());
        assertEquals("l4", leagues.get(1).getName());
        assertEquals("l2", leagues.get(2).getName());
        assertEquals("l1", leagues.get(3).getName());
        assertEquals("l3", leagues.get(4).getName());
    }

}
