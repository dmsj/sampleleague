/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util;

import com.jobtask.league.model.League;
import com.jobtask.league.model.Player;
import com.jobtask.league.model.Team;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmsj
 */
public class RankingUtilTest {

    List<Player> _players;
    List<Team> _teams;
    List<League> _leagues;

    public RankingUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        _teams = new ArrayList<>();
        _players = new ArrayList<>();
        _leagues = new ArrayList<>();
        _players = new ArrayList<>();
        //init players
        Player p1 = new Player();
        p1.setScore(10);
        p1.setPlayedGames(2);
        p1.setName("p1");
        Player p2 = new Player();
        p2.setScore(10);
        p2.setPlayedGames(2);
        p2.setName("p2");
        Player p3 = new Player();
        p3.setPlayedGames(8);
        p3.setScore(11);
        p3.setName("p3");
        Player p4 = new Player();
        p4.setScore(5);
        p4.setName("p4");
        p4.setPlayedGames(1);
        _players.add(p1);
        _players.add(p2);
        _players.add(p3);
        _players.add(p4);

        //init leagues
        League l1 = new League();
        l1.setName("l1");
        l1.setTotalScore(9);
        League l2 = new League();
        l2.setTotalScore(9);
        l2.setName("l2");
        League l3 = new League();
        l3.setTotalScore(8);
        l3.setName("l3");
        League l4 = new League();
        l4.setTotalScore(10);
        l4.setName("l4");
        l1.setAverageScore(10);
        l2.setAverageScore(5);
        l3.setAverageScore(10);
        l4.setAverageScore(10);
        _leagues.add(l1);
        _leagues.add(l2);
        _leagues.add(l3);
        _leagues.add(l4);

        //initialize teams
        Team tm1 = new Team();
        Team tm2 = new Team();
        Team tm3 = new Team();
        Team tm4 = new Team();
        tm1.setTotalScore(5);
        tm2.setTotalScore(8);
        tm3.setTotalScore(8);
        tm4.setTotalScore(9);
        tm1.setName("tm1");
        tm2.setName("tm2");
        tm3.setName("tm3");
        tm4.setName("tm4");
        tm1.setPlayedGames(1);
        tm2.setPlayedGames(2);
        tm3.setPlayedGames(3);
        tm4.setPlayedGames(1);
        _teams.add(tm1);
        _teams.add(tm2);
        _teams.add(tm3);
        _teams.add(tm4);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of resolveTeamsByScore method, of class RankingUtil.
     */
    @Test
    public void testResolveTeamsByScore() {
        System.out.println("resolveTeamsByScore");
        List<Team> teams = _teams;
        int limit = 3;

        //test ordering before sorting
        assertEquals("tm1", teams.get(0).getName());
        assertEquals("tm4", teams.get(teams.size() - 1).getName());

        RankingUtil.resolveTeamsByScore(teams, true, 0);

        //test ordering after sorting
        assertEquals("tm4", teams.get(0).getName());
        assertEquals("tm1", teams.get(teams.size() - 1).getName());

        //test ranking
        assertEquals("1", teams.get(0).getRank());
        assertEquals("2-3", teams.get(1).getRank());
        assertEquals("2-3", teams.get(2).getRank());
        assertEquals("4", teams.get(3).getRank());

        //test oversized lists resizer
        RankingUtil.resolveTeamsByScore(teams, false, limit);
        assertTrue(limit >= teams.size());
    }

    /**
     * Test of resolveTeamsByAverageScore method, of class RankingUtil.
     */
    @Test
    public void testResolveTeamsByAverageScore() {
        System.out.println("resolveTeamsByAverageScore");
        List<Team> teams = _teams;
        int limit = 3;
        //test order before soring
        assertEquals("tm1", teams.get(0).getName());
        assertEquals("tm4", teams.get(teams.size() - 1).getName());

        RankingUtil.resolveTeamsByAverageScore(teams, true, 0);

        //test order after sorting
        assertEquals("tm4", teams.get(0).getName());
        assertEquals("tm3", teams.get(teams.size() - 1).getName());

        //test ranking
        assertEquals("1", teams.get(0).getRank());
        assertEquals("2", teams.get(1).getRank());
        assertEquals("3", teams.get(2).getRank());
        assertEquals("4", teams.get(3).getRank());

        //test oversized list resizer
        RankingUtil.resolveTeamsByAverageScore(teams, false, limit);
        assertTrue(limit >= teams.size());
    }

    /**
     * Test of resolvePlayersByScore method, of class RankingUtil.
     */
    @Test
    public void testResolvePlayersByScore() {
        System.out.println("resolvePlayersByScore");
        List<Player> players = _players;
        RankingUtil.resolvePlayersByScore(players, true, 0);

        //test ranks
        assertEquals("1", players.get(0).getRank());
        assertEquals("2-3", players.get(1).getRank());
        assertEquals("2-3", players.get(2).getRank());
        assertEquals("4", players.get(3).getRank());

        int limit = 4;
        RankingUtil.resolvePlayersByScore(players, false, limit);
        //test oversized list resizer
        assertEquals(limit, players.size());
    }

    /**
     * Test of resolvePlayersByAverageScore method, of class RankingUtil.
     */
    @Test
    public void testResolvePlayersByAverageScore() {
        System.out.println("resolvePlayersByAverageScore");
        List<Player> players = _players;

        RankingUtil.resolvePlayersByAverageScore(players, true, 0);

        //test ranks
        assertEquals("1-2-3", players.get(0).getRank());
        assertEquals("1-2-3", players.get(1).getRank());
        assertEquals("1-2-3", players.get(2).getRank());
        assertEquals("4", players.get(3).getRank());

        int limit = 1;
        RankingUtil.resolvePlayersByAverageScore(players, false, limit);
        //test oversized list resizer
        assertEquals(limit, players.size());
    }

    /**
     * Test of resolveLeaguesByScore method, of class RankingUtil.
     */
    @Test
    public void testResolveLeaguesByScore() {
        System.out.println("resolveLeaguesByScore");
        List<League> leagues = _leagues;
        int limit = 5;
        RankingUtil.resolveLeaguesByScore(leagues, true, 0);
        //test ranking
        assertEquals("1", leagues.get(0).getRank());
        assertEquals("2-3", leagues.get(1).getRank());
        assertEquals("2-3", leagues.get(2).getRank());
        assertEquals("4", leagues.get(3).getRank());

        RankingUtil.resolveLeaguesByScore(leagues, false, limit);
        //test oversized list resizer
        assertTrue(limit >= leagues.size());
    }

    /**
     * Test of resolveLeaguesByAverageScore method, of class RankingUtil.
     */
    @Test
    public void testResolveLeaguesByAverageScore() {
        System.out.println("resolveLeaguesByAverageScore");
        List<League> leagues = _leagues;
        int limit = 1;
        RankingUtil.resolveLeaguesByAverageScore(leagues, true, 0);
        //test ranking
        assertEquals("1-2-3", leagues.get(0).getRank());
        assertEquals("1-2-3", leagues.get(1).getRank());
        assertEquals("1-2-3", leagues.get(2).getRank());
        assertEquals("4", leagues.get(3).getRank());

        RankingUtil.resolveLeaguesByAverageScore(leagues, false, limit);
        //test oversized list resizer
        assertTrue(limit >= leagues.size());
    }

}
