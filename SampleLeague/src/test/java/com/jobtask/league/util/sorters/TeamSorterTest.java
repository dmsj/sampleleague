/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jobtask.league.util.sorters;

import com.jobtask.league.model.Team;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dmsj
 */
public class TeamSorterTest {

    List<Team> _teams;

    public TeamSorterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        _teams = new ArrayList<>();
        Team tm1 = new Team();
        Team tm2 = new Team();
        Team tm3 = new Team();
        Team tm4 = new Team();
        tm1.setName("tm1");
        tm2.setName("tm2");
        tm3.setName("tm3");
        tm4.setName("tm4");
        tm1.setTotalScore(5);
        tm2.setTotalScore(10);
        tm3.setTotalScore(2);
        tm4.setTotalScore(8);
        tm1.setPlayedGames(1);
        tm2.setPlayedGames(3);
        tm3.setPlayedGames(4);
        tm4.setPlayedGames(1);
        _teams.add(tm1);
        _teams.add(tm2);
        _teams.add(tm3);
        _teams.add(tm4);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sortByTotalPlayersScore method, of class TeamSorter.
     */
    @Test
    public void testSortByTotalPlayersScore() {
        System.out.println("sortByTotalPlayersScore");
        List<Team> teams = _teams;
        teams.sort(TeamSorter::sortByTotalPlayersScore);
        assertEquals("tm2", teams.get(0).getName());
        assertEquals("tm4", teams.get(1).getName());
        assertEquals("tm1", teams.get(2).getName());
        assertEquals("tm3", teams.get(3).getName());
    }

    /**
     * Test of sortByAverageScore method, of class TeamSorter.
     */
    @Test
    public void testSortByAverageScore() {
        System.out.println("sortByAverageScore");
        List<Team> teams = _teams;
        teams.sort(TeamSorter::sortByAverageScore);
        assertEquals("tm4", teams.get(0).getName());
        assertEquals("tm1", teams.get(1).getName());
        assertEquals("tm2", teams.get(2).getName());
        assertEquals("tm3", teams.get(3).getName());
    }

}
