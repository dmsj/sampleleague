### What is this repository for? ###

CRUD operations using Hibernate/JPA, MySQL.

Front-end JSF/PrimeFaces.

Back-end EJB, CDI.

Build tool - Maven.

Testing framework - JUnit.


### How do I get set up? ###

Configure your database properties in hibernate.cfg.xml file.

Add sql files to your database. 

### Who do I talk to? ###

Andrius Vaitkus